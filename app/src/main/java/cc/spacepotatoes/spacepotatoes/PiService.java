package cc.spacepotatoes.spacepotatoes;

import java.io.Serializable;

/**
 * Created by Socati on 2015-05-14.
*/
public interface PiService {
    public int connect(String user, String password);
    public int disconnect(String name);
    public String getChatMsg(String name);
    public void setChatMsg(String data, boolean speakIt, String name);
}
