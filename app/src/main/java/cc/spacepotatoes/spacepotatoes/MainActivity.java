package cc.spacepotatoes.spacepotatoes;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity{

    //Login Information sent to the server
    private String ip;
    private String name;
    private String password;
    private String port;

    //this Activity
    private MainActivity ma = this;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        setTitle(R.string.title_activity_main);

        //all components
        final TextView ipTV = (TextView)findViewById(R.id.ip);
        ipTV.setText("n-abel.de");
        final TextView nameTV = (TextView)findViewById(R.id.name);
        final TextView passwordTV = (TextView)findViewById(R.id.password);
        final TextView portTV = (TextView)findViewById(R.id.port);
        final Button con = (Button)findViewById(R.id.connect);
        con.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get all Login Information and check if it is correct
                name = nameTV.getText().toString();
                if (name.equals("")) {
                    showSimplePopUp("Warning", "Please enter a user name.");
                    return;
                }
                password = passwordTV.getText().toString();
                ip = ipTV.getText().toString();
                port = portTV.getText().toString();
                if (port.equals("")) {
                    port = "44444";
                }

                //create the new Activity and give it the Login Information
                Intent intent = new Intent(ma, ChatActivity.class);
                intent.putExtra("IP", ip);
                intent.putExtra("name", name);
                intent.putExtra("password", password);
                intent.putExtra("port", port);
                startActivity(intent);
            }
        });

        final Button books = (Button) findViewById(R.id.books);
        books.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ma, BookmarkActivity.class);
                startActivity(intent);
            }
        });

        //Create an error if this activity was restarted after something went wrong
        String status = getIntent().getStringExtra("error");
        System.out.println(status);
        if (status != null) {
            switch (status) {
                case "error":
                    showSimplePopUp("Error", "Something went wrong, please try again.");
                    break;
                case "password":
                    showSimplePopUp("Error", "Wrong password.");
                    break;
                case "user":
                    showSimplePopUp("Error", "There is already a User with this name online.");
                    break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.infos:
                //show the credits
                String version = null;
                try {
                    version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                showSimplePopUp("Credits", "Version: " + version + "\n" +
                        "By:\n" +
                        "Caspar Wiswesser\n" +
                        "Vincent Stollenwerk\n" +
                        "Pavitter Ghotra\n" +
                        "Christian Almeida Sousa\n" +
                        "Niklas Abel");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showSimplePopUp(String title, String text) {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        helpBuilder.setTitle(title);
        helpBuilder.setMessage(text);
        helpBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}
                });

        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();
    }
}
