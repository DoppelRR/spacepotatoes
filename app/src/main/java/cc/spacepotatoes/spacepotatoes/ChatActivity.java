package cc.spacepotatoes.spacepotatoes;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import lipermi.handler.CallHandler;
import lipermi.net.Client;


public class ChatActivity extends ActionBarActivity {

    //Login information
    private String ip;
    private String name;
    private String password;
    private String port;

    //The current state of the text
    private String lastText = "";

    //Serverconnection
    private Client client;
    private PiService piService = null;
    private int connected = 0;
    private final Lock _mutex = new ReentrantLock(true);
    
    private Timer timer;

    private ChatActivity ca = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chat);

        //all Components
        final TextView message = (TextView)findViewById(R.id.message);
        final TextView chat = (TextView)findViewById(R.id.chat);
        final ScrollView sv = (ScrollView)findViewById(R.id.scrollView);
        sv.post(new Runnable() {
            @Override
            public void run() {
                sv.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
        final ImageButton send = (ImageButton) findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (connected == 0) {
                    showSimplePopUp("Warning", "Still connecting, maybe the IP was wrong, press disconnect to try again.");
                    return;
                }
                _mutex.lock();
                piService.setChatMsg(name+ ": " + message.getText().toString(), false, name);
                _mutex.unlock();
                message.setText("");
            }

        });
        final ImageButton speak = (ImageButton)findViewById(R.id.speak);
        speak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (connected == 0) {
                    showSimplePopUp("Warning", "Still connecting, maybe the IP was wrong, press disconnect to try again.");
                    return;
                }
                _mutex.lock();
                piService.setChatMsg(name + ": " + message.getText().toString(), true, name);
                _mutex.unlock();
                message.setText("");
            }
        });

        //get the Login Information
        ip = getIntent().getStringExtra("IP");
        name = getIntent().getStringExtra("name");
        password = getIntent().getStringExtra("password");
        port = getIntent().getStringExtra("port");

        //establish a Server connection
        new Conn().execute();

        chat.setText("Connecting, please wait.");
        chat.setText("");
        //updates the chat frequently
        final Handler handler = new Handler();
        timer = new Timer("RECEIVE_TIMER");
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (connected == 1) {
                        _mutex.lock();
                        String text = piService.getChatMsg(name);
                        _mutex.unlock();
                        if (!text.equals(lastText)) {
                            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                            v.vibrate(400);
                            lastText = text;
                            chat.setText(chat.getText() + text);
                            final ScrollView sv = (ScrollView) findViewById(R.id.scrollView);
                            sv.post(new Runnable() {
                                @Override
                                public void run() {
                                    sv.fullScroll(ScrollView.FOCUS_DOWN);
                                }
                            });
                        }
                    }
                }
            });
            }
        }, 0, 1000);
    }

    @Override
    protected void onPause() { //called when the menu back Button is pressed
        try {
            if (client != null) {
                _mutex.lock();
                piService.disconnect(name);
                client.close();
                _mutex.unlock();
            }
            timer.cancel();
            Intent intent = new Intent(ca, MainActivity.class);
            startActivity(intent);
        } catch(IOException e) {}
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }



    class Conn extends AsyncTask<Void, Void, MainActivity> {
        @Override
        protected MainActivity doInBackground(Void... params) {
            try {
                CallHandler callHandler = new CallHandler();
                client = new Client(ip, Integer.parseInt(port), callHandler);
                piService = (PiService) client.getGlobal(PiService.class);
                connected = piService.connect(name, password); //TODO version check
                System.out.println(connected);
                if (connected == -1)  {
                    Intent intent = new Intent(ca, MainActivity.class);
                    intent.putExtra("error", "password");
                    startActivity(intent);
                } else if (connected == -2) {
                    Intent intent = new Intent(ca, MainActivity.class);
                    intent.putExtra("error", "user");
                    startActivity(intent);
                }
            } catch (IOException ioe) {
                Intent intent = new Intent(ca, MainActivity.class);
                intent.putExtra("error", "error");
                startActivity(intent);
                ioe.printStackTrace();
            }
            return null;
        }

    }

    private void showSimplePopUp(String title, String text) {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        helpBuilder.setTitle(title);
        helpBuilder.setMessage(text);
        helpBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog
                    }
                });

        // Remember, create doesn't show the dialog
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();
    }
}
