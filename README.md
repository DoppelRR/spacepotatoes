# README #

#Commits:#
* This is an explanation which syntax I use for commits.
* "someNamehere:" always is a subpoint, mostly the name of the class which the following points refer to
* "+ somethingHere": the somethingHere was added
* "- somethingHere": the somethingHere was removed
* "! somethingHere": the somethingHere still has a bug/problem
* "# somethingHere": a solution for a problem in somethingHere
* "*" everything else